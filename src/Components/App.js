import React, { Component } from "react";
import ReactDOM from "react-dom";
import Table from "./Table";
import SearchBar from "./SearchBar";
import data from "../data";

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: data
    };
  }

  updateState(data) {
    this.setState({
      users: data
    });
  }

  render() {
    return (
      <div className="container">
        <SearchBar
          users={this.state.users}
          updateState={this.updateState.bind(this)}
        />
        <Table
          users={this.state.users}
          toggle={this.state.toggle}
          updateState={this.updateState.bind(this)}
        />
      </div>
    );
  }
}
