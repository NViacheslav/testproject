import React, { Component } from "react";

export default class Table extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeColumn: 0,
      toggle: true
    };
  }

  capitalLetter() {
    return this.charAt(0).toUpperCase() + this.slice(1);
  }

  clickHandler(title, key) {
    if (this.state.activeColumn === key) {
      let toggle = !this.state.toggle;
      const data = sortByColumn(this.props.users, title, toggle);
      this.setState({
        toggle: toggle,
        activeColumn: key
      });
      this.props.updateState(data);
    } else {
      const data = sortByColumn(this.props.users, title, false);
      this.props.updateState(data);
      this.setState({
        activeColumn: key
      });
    }

    function sortByColumn(data, colIndex, reverse) {
      {
        if (reverse == true) {
          data.sort(sortFunction).reverse();
        } else {
          data.sort(sortFunction);
        }
        function sortFunction(a, b) {
          if (a[colIndex] === b[colIndex]) {
            return 0;
          } else {
            return a[colIndex] < b[colIndex] ? -1 : 1;
          }
        }
        return data;
      }
    }
  }

  render() {
    const { users } = this.props;
    return (
      <table className="responsive-table">
        <thead>
          <tr>
            {Object.keys(this.props.users[0]).map((title, key) => {
              return (
                <th
                  key={key}
                  onClick={() => this.clickHandler(title, key)}
                  scope="col"
                  data-label={title}
                >
                  {title.toUpperCase()}
                  {this.state.activeColumn === key
                    ? this.state.toggle
                      ? " ↓"
                      : " ↑"
                    : ""}
                </th>
              );
            })}
          </tr>
        </thead>
        <tbody>
          {this.props.users.map(function(row, key) {
            return (
              <tr key={key}>
                {Object.keys(row).map(function(entry, key) {
                  return (
                    <td scope="row" key={key} data-label={entry}>
                      {row[entry]}
                    </td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }
}
