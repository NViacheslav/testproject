import React, { Component } from "react";

export default class SearchBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: this.props.users,
      term: ""
    };
    this.dataSearch = this.dataSearch.bind(this);
  }

  dataSearch(e) {
    this.setState({ term: e.target.value });
    const value = e.target.value.toLowerCase();
    const filter = this.state.users.filter(user => {
      return user.name.toLowerCase().includes(value);
    });
    if (filter.length == 0) {
      return 0;
    } else {
      this.props.updateState(filter);
    }
  }

  render() {
    return (
      <div className="input-group">
        <form>
          Search employee:
          <input
            // placeholder=""
            className="form-control"
            value={this.state.term}
            onChange={this.dataSearch}
          />
        </form>
      </div>
    );
  }
}
